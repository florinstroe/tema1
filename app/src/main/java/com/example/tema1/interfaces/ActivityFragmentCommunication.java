package com.example.tema1.interfaces;

public interface ActivityFragmentCommunication {
    void openSecondActivity();
    void addF2A2();
    void replaceWithF3A2();
    void returnToF1A2();
    void closeTheActivity();
}