package com.example.tema1.activities

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tema1.R
import com.example.tema1.interfaces.ActivityFragmentCommunication

class MainActivity : AppCompatActivity(), ActivityFragmentCommunication {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun openSecondActivity() {
        val intent = Intent(this, Activity2::class.java)
        startActivity(intent)
        this.finish();
    }

    override fun addF2A2() {
        TODO("Not yet implemented")
    }

    override fun replaceWithF3A2() {
        TODO("Not yet implemented")
    }

    override fun returnToF1A2() {
        TODO("Not yet implemented")
    }

    override fun closeTheActivity() {
        TODO("Not yet implemented")
    }
}