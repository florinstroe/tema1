package com.example.tema1.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;

import com.example.tema1.R;
import com.example.tema1.fragments.F1A2;
import com.example.tema1.fragments.F2A2;
import com.example.tema1.fragments.F3A2;
import com.example.tema1.interfaces.ActivityFragmentCommunication;

public class Activity2 extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        addF1A2();
    }

    public void addF1A2() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F1A2.class.getName();
        FragmentTransaction addTransaction = transaction.add(
                R.id.frame_layout, new F1A2(), tag
        );
        addTransaction.commit();
    }

    public void addF2A2() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F2A2.class.getName();
        FragmentTransaction replaceTransaction = transaction.replace(
                R.id.frame_layout, new F2A2(), tag
        );
        replaceTransaction.addToBackStack(tag);
        replaceTransaction.commit();
    }

    public void replaceWithF3A2() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String tag = F3A2.class.getName();
        FragmentTransaction replaceTransaction = transaction.replace(
                R.id.frame_layout, new F3A2(), tag
        );
        replaceTransaction.addToBackStack(tag);
        replaceTransaction.commit();
    }

    public void returnToF1A2() {
        this.onBackPressed();
    }

    @Override
    public void closeTheActivity() {
        this.finish();
    }

    @Override
    public void openSecondActivity() {
    }
}