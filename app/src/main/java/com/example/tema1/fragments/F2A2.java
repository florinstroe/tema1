package com.example.tema1.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tema1.R;
import com.example.tema1.interfaces.ActivityFragmentCommunication;

public class F2A2 extends Fragment {
    @Nullable
    ActivityFragmentCommunication activityFragmentCommunication;

    public static F2A2 newInstance() {

        Bundle args = new Bundle();

        F2A2 fragment = new F2A2();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_f2_a2, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button F2A2Button1 = view.findViewById(R.id.f2_a2_button1);
        F2A2Button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityFragmentCommunication.replaceWithF3A2();
            }
        });

        Button F2A2Button2 = view.findViewById(R.id.f2_a2_button2);
        F2A2Button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityFragmentCommunication.returnToF1A2();
            }
        });

        Button F2A2Button3 = view.findViewById(R.id.f2_a2_button3);
        F2A2Button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityFragmentCommunication.closeTheActivity();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityFragmentCommunication) {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }
}
