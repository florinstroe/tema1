package com.example.tema1.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.tema1.R;
import com.example.tema1.interfaces.ActivityFragmentCommunication;

public class F1A2 extends Fragment {
    @Nullable
    ActivityFragmentCommunication activityFragmentCommunication;

    public static F1A2 newInstance() {
        
        Bundle args = new Bundle();
        
        F1A2 fragment = new F1A2();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_f1_a2, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Button F1A2Button = view.findViewById(R.id.f1_a2_button);
        F1A2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activityFragmentCommunication.addF2A2();
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityFragmentCommunication) {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }
}
